<?php

/**
 * @file
 * Contains \Drupal\migrate_example\Plugin\migrate\source\BeerNode.
 */

namespace Drupal\migrate_webform\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Source plugin publication content type.
 *
 * @MigrateSource(
 *   id = "webform_node"
 * )
 */
class WebformNode extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    /**
     * An important point to note is that your query *must* return a single row
     * for each item to be imported. Here we might be tempted to add a join to
     * migrate_example_beer_topic_node in our query, to pull in the
     * relationships to our categories. Doing this would cause the query to
     * return multiple rows for a given node, once per related value, thus
     * processing the same node multiple times, each time with only one of the
     * multiple values that should be imported. To avoid that, we simply query
     * the base node data here, and pull in the relationships in prepareRow()
     * below.
     */
    $serialized_webform_node_types = $this->select('variable', 'variable')
      ->fields('variable', ['value'])
      ->condition('name', 'webform_node_types', '=')
      ->execute()
      ->fetchAll();
    $webform_node_types = unserialize($serialized_webform_node_types[0]['value']);

    $query = $this->select('node', 'node')
      ->fields('node')
      ->condition('type', $webform_node_types, 'IN');
    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'nid' => $this->t('The primary identifier for a node.'),
      'vid' => $this->t('The current node_revision.vid version identifier.'),
      'type' => $this->t('The node_type.type of this node.'),
      'language' => $this->t('The languages.language of this node.'),
      'title' => $this->t('The title of this node, always treated as non-markup plain text.'),
      'uid' => $this->t('The users.uid that owns this node; initially, this is the user that created it.'),
      'status' => $this->t('Boolean indicating whether the node is published (visible to non-administrators).'),
      'created' => $this->t('The Unix timestamp when the node was created.'),
      'changed' => $this->t('The Unix timestamp when the node was most recently saved.'),
      'comment' => $this->t('Whether comments are allowed on this node: 0 = no, 1 = closed (read only), 2 = open (read/write).'),
      'promote' => $this->t('Boolean indicating whether the node should be displayed on the front page.'),
      'sticky' => $this->t('Boolean indicating whether the node should be displayed at the top of lists in which it appears.'),
      'tnid' => $this->t('The translation set id for this node, which equals the node id of the source post in each set.'),
      'translate' => $this->t('A boolean indicating whether this translation page needs to be updated.'),
      'uuid' => $this->t('The Universally Unique Identifier.'),
      // Extra fields
      'machine_name' => $this->t('Machine name derived from title'),
      'recipients' => $this->t('Recipients from webform_email table'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
        'alias' => 'node',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // create a machine name from the title.
    $title = $row->getSourceProperty('title');
    $machine_name = strtolower($title);
    $machine_name = preg_replace('@[^a-z0-9_]+@','_',$machine_name);
    $machine_name = substr($machine_name, 0, 32);
    $row->setSourceProperty('machine_name', $machine_name);

    // Add recipients from webform_email table.
    $recipients = $this->select('webform_emails', 'webform_emails')
      ->fields('webform_emails', ['email'])
      ->condition('nid', $row->getSourceProperty('nid'))
      ->execute()
      ->fetchCol();
    // Remove invalid email-addresses.
    foreach($recipients as $id => $recipient){
      if(!\Drupal::service('email.validator')->isValid($recipient)){
        unset($recipients[$id]);
      }
    }
    $row->setSourceProperty('recipients', $recipients);
    return parent::prepareRow($row);
  }

}
