<?php

/**
 * @file
 * Contains \Drupal\migrate_example\Plugin\migrate\source\BeerNode.
 */

namespace Drupal\migrate_webform\Plugin\migrate\source;

use Drupal\migrate\Plugin\migrate\source\SqlBase;
use Drupal\migrate\Row;

/**
 * Source plugin to map Webform Components to fields
 *
 * @MigrateSource(
 *   id = "webform_component_field"
 * )
 */
class WebformComponentField extends SqlBase {

  /**
   * {@inheritdoc}
   */
  public function query() {
    /**
     * An important point to note is that your query *must* return a single row
     * for each item to be imported. Here we might be tempted to add a join to
     * migrate_example_beer_topic_node in our query, to pull in the
     * relationships to our categories. Doing this would cause the query to
     * return multiple rows for a given node, once per related value, thus
     * processing the same node multiple times, each time with only one of the
     * multiple values that should be imported. To avoid that, we simply query
     * the base node data here, and pull in the relationships in prepareRow()
     * below.
     */
    $query = $this->select('webform_component', 'component');
    $query->leftJoin('node', 'node', 'node.nid = component.nid');
    $query->fields('component')
      ->fields('node', ['title'])
      ->condition('component.type', array('hidden', 'mailchimp', 'fieldset'), 'NOT IN');

    return $query;
  }

  /**
   * {@inheritdoc}
   */
  public function fields() {
    $fields = [
      'nid' => $this->t('The node identifier of a webform.'),
      'cid' => $this->t('The identifier for this component within this node, starts at 0 for each node.'),
      'pid' => $this->t('If this component has a parent fieldset, the cid of that component.'),
      'form_key' => $this->t('When the form is displayed and processed, this key can be used to reference the results.'),
      'name' => $this->t('The label for this component.'),
      'type' => $this->t('The field type of this component (textfield, select, hidden, etc.).'),
      'value' => $this->t('The default value of the component when displayed to the end-user.'),
      'extra' => $this->t('Additional information unique to the display or processing of this component.'),
      'mandatory' => $this->t('Boolean flag for if this component is required.'),
      'weight' => $this->t('Determines the position of this component in the form.'),
      // Extra fields
      'machine_name' => $this->t('Machine name derived from title'),
      'field_name' => $this->t('Field name derived from form_key'),
    ];

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getIds() {
    return [
      'nid' => [
        'type' => 'integer',
        'alias' => 'component',
      ],
      'cid' => [
        'type' => 'integer',
        'alias' => 'component',
      ],
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function prepareRow(Row $row) {
    // create a machine name from the title.
    $title = $row->getSourceProperty('title');
    $machine_name = strtolower($title);
    $machine_name = preg_replace('@[^a-z0-9_]+@','_',$machine_name);
    $machine_name = substr($machine_name, 0, 32);
    $row->setSourceProperty('machine_name', $machine_name);

    $row->setSourceProperty('field_name', preg_replace('@[^a-z0-9_]+@','_',substr($row->getSourceProperty('type') . '_' . $row->getSourceProperty('form_key'), 0, 32)));
    return parent::prepareRow($row);
  }

}
